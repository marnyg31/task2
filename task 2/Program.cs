﻿using System;
using System.Reflection;

namespace task_2
{
    class Program
    {

        static void printRect(int sqrWitdh, int sqrHeigt)
        {
            //Function that draws a square by nesting for loops
            for (int i = 0; i < sqrWitdh; i++)
            {
                for (int j = 0; j < sqrHeigt; j++)
                {
                    writeCorrectCharForPos(sqrWitdh, sqrHeigt, i, j);
                }
                Console.WriteLine();
            }
        }

        private static void writeCorrectCharForPos(int sqrWitdh, int sqrHeigt, int i, int j)
        {
            //Function that decides on whitch char to write to consloe based on i and j of current itteratin
            if (i == 0 || i == sqrWitdh - 1 || j == 0 || j == sqrHeigt - 1)
            {
                Console.Write("#");
            }
            else
            {
                Console.Write(" ");
            }
        }

        static void Main(string[] args)
        {
            //First we draw a square with equal sides
            //to do this we get a input from the user and check that it is a number
            Console.WriteLine("how large do you want the square");
            Console.WriteLine("please enter one integer number:");
            int sqrSize;
            if (int.TryParse(Console.ReadLine(), out sqrSize))
            {
                printRect(sqrSize, sqrSize);
            }
            else
            {
                Console.WriteLine("you didn't enter an integer number");
            }


            //Now we draw a rectangle with differing sides
            //to do this we get two inputs from the user and check that it they are number
            Console.WriteLine("how large do you want the rectangle");
            Console.WriteLine("please enter two integer numbers:");
            int sqrWidth;
            int sqrHeight;
            if (int.TryParse(Console.ReadLine(), out sqrWidth) && int.TryParse(Console.ReadLine(), out sqrHeight))
            {
                printRect(sqrWidth, sqrHeight);
            }
            else
            {
                Console.WriteLine("you didn't enter two integer number");
            }

        }
    }
}
